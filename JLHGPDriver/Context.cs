﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JLHGoldEngine;

namespace JLHGPDriver
{
	public class Context
	{

		public Context()
		{
		}

		public string GetTokenText()
		{
			switch (V.tokenSymbolIdx)
			{
				case (int)SymbolConstants.symbol_eof :
					return null;
				case (int)SymbolConstants.symbol_minus :
				case (int)SymbolConstants.symbol_lparen :
				case (int)SymbolConstants.symbol_rparen :
				case (int)SymbolConstants.symbol_mult :
				case (int)SymbolConstants.symbol_comma :
				case (int)SymbolConstants.symbol_div :
				case (int)SymbolConstants.symbol_plus :
				case (int)SymbolConstants.symbol_avg :
				case (int)SymbolConstants.symbol_cellref :
				case (int)SymbolConstants.symbol_intconstant :
				case (int)SymbolConstants.symbol_sum :
				{
					return V.tokenText;
				}

				default:
					Debug.WriteLine("UNHANDLED SYMBOL: {0} {1}", V.tokenText, V.tokenSymbolIdx);
					throw new Exception("You don't want the text of a non-terminal symbol");

			}
		}


	}

	public class CellRefList
	{
		private readonly Dictionary<string, object> _varList = new Dictionary<string, object>();

		public bool Add(string name, object value)
		{
			string upper = name.ToUpper();
			if (_varList.ContainsKey(upper))
				return false;
			_varList.Add(upper, value);
			return true;
		}

		public void ClearValues()
		{
			_varList.Clear();
		}

		public int Count => _varList.Count;

		public object this[string name]
		{
			get => _varList[name];
			set => _varList[name] = value;
		}

		private class Variable
		{
			private readonly string _name;
			private object _value;


			public Variable(string name, object value)
			{
				_name = name;
				_value = value;
			}
		}
	}


}
