﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JLHGoldEngine;
using Microsoft.SqlServer.Server;

namespace JLHGPDriver
{
	class Program
	{
		private static StringBuilder dotGraphBuilder = new StringBuilder("digraph AST\r\n{\r\n");
		private static int letterIndex = 0;
		const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		//=========================================================================================
		static void Main(string[] args)
		{
			GPLoader loader = new GPLoader(V.GPXML_FILE);
			loader.Load();

			// Initialize
//			V.lastLalrResult = LalrResult.Null;
			V.curLalrState = 0;

			V.tokenLength = 0;
			V.tokenSymbolIdx = 0;
			V.tokenSymbolType = SymbolType.Null;
			V.tokenData = 0;
//			V.tokenState = 0;
			V.tokenFilled = false;
			V.tokenText = "";


			//Tokenize();


			Token rootToken = new Token(0,"ROOT", SymbolType.Null);
//			rootToken.Data = new Reduction();
			V.TokenStack.Push(rootToken);

			LALR1Parse();
		}
		//=========================================================================================


		//=========================================================================================
		private static void LALR1Parse()
		{
			while (true)
			{
				LALR1Parser.ParseLALR1();

				if (V.parseMessage == ParseMessage.TokenRead)
				{
					continue;
				}

				if (V.parseMessage == ParseMessage.ParseAccept)
				{
					Console.WriteLine("MAIN PARSE ACCEPT");
					//Console.WriteLine(V.INPUT_BUFFER);
					//Token t = V.TokenStack.Peek();
					//Console.WriteLine();
					//Console.WriteLine("  production {0} {1}", t.Index, t.Text);
					//Reduction r = (Reduction)t.Data;
					//PrintReduction(1, r);
					//Console.WriteLine();
					//CreateDot(1, r);
					//dotGraphBuilder.Append("}");
					//Console.WriteLine(dotGraphBuilder.ToString());
					//PrintSymbols(r);
					break;
				}

				if (V.parseMessage == ParseMessage.SyntaxError)
				{
					Console.WriteLine("MAIN PARSE ERROR at " + V.INPUT_BUFFER[V.inputBufPtr]);
					break;
				}

				if (V.parseMessage == ParseMessage.LexEmpty)
				{
					Console.WriteLine("EMPTY FORMULA");
					break;
				}


			}

			Console.ReadKey();
		}
		//=========================================================================================


		//=========================================================================================
		public static void PrintReduction(int indent, Reduction reduction)
		{
			int indentlevel = indent;
			const string oneindent = "\u2500\u2500\u2500\u2500\u2524";
			const string oneindent2 = "\u00B7\u00B7\u00B7\u00B7\u00B7";
			StringBuilder sb1 = new StringBuilder();
			StringBuilder sb2 = new StringBuilder();
			sb1.Append("  ");
			sb2.Append("  ");

			for (int n = 1; n <= indentlevel; n++)
			{ 
				sb1.Append(oneindent);
				sb2.Append(oneindent2);
			}

			string expansion = V.ProductionList.Productions[reduction.Production.Index].Expansion;
			Console.WriteLine(sb1.ToString() + " prod " + reduction.Production.Index + "  " + expansion);

			foreach (Token t1 in reduction.Tokens)
			{
				if (t1.Data != null)
				{
					if (t1.Data.GetType() == typeof(Reduction))
					{
						indentlevel++;
						Reduction r2 = (Reduction)t1.Data;
						PrintReduction(indentlevel, r2);
					}
				}
				else
				{
					Console.WriteLine(sb2.ToString() + " " + t1.Name);
				}
			}
		}
		//=========================================================================================


		//=========================================================================================
		public static void CreateDot(int level, Reduction reduction)
		{
			string thisRoot = letters[letterIndex] + " -> ";

			PrintSymbols(reduction);

			foreach (Token t1 in reduction.Tokens)
			{
				dotGraphBuilder.Append(thisRoot);

				if (t1.Data != null)
				{
					if (t1.Data.GetType() == typeof(Reduction))
					{
						level++;
						Reduction r2 = (Reduction)t1.Data;
						CreateDot(level, r2);
					}
				}
				else
				{
					dotGraphBuilder.Append("\"" + t1.Name + "\"" + "\r\n");
					letterIndex++;
				}
			}
		}

		//=========================================================================================


		//=========================================================================================
		public static void PrintSymbols(Reduction reduction)
		{
			foreach (var sym in reduction.Production.ProductionSymbols)
			{
				string symName = V.SymbolsList.List[sym].Name;
				string symValue = reduction.Production.NonTerminalIndex.ToString();
				Console.WriteLine(symName + " " + symValue);
			}
		}

		//=========================================================================================


		//=========================================================================================
		//private static void Tokenize()
		//{
		//	while (true)
		//	{
		//		LALR1Parser.Tokenize();

		//		if (V.parseMessage == ParseMessage.LexAccept)
		//		{
		//			Console.WriteLine("MAIN LEX ACCEPT");
		//			foreach (var token in V.TokenList)
		//			{
		//				Console.WriteLine(token.Text + "\t" + token.Index + "\t" + token.SymbolType);
		//			}
		//			goto exitTokenize;
		//		}

		//		if (V.parseMessage == ParseMessage.LexicalError)
		//		{
		//			Console.WriteLine("MAIN LEXICAL ERROR");
		//			goto exitTokenize;
		//		}

		//		if (V.parseMessage == ParseMessage.LexEmpty)
		//		{
		//			Console.WriteLine("EMPTY");
		//			goto exitTokenize;
		//		}
		//	}
		//exitTokenize:
		//	Console.ReadKey();
		//}
		//=========================================================================================

	}
}
