﻿using System.Collections.Generic;

namespace JLHGoldEngine
{
	public class Charset
	{
		public int Index
		{
			get; protected set;
		}

		public int Count
		{
			get; protected set;
		}

		public List<char> Chars { get; protected set; }

		public Charset(int index, int count)
		{
			Index = index;
			Count = count;
			Chars = new List<char>();
		}

	}



	public class CharsetList
	{
		public List<Charset> List { get; set; }

		public CharsetList()
		{
			List = new List<Charset>();
		}
	}
}
