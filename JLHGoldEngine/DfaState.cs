﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGoldEngine
{
	public class DfaState
	{
		public int Index { get; protected set; }
		public int AcceptSymbol { get; protected set; }
		public int EdgeCount { get; protected set; }
		public List<Edge> Edges { get; protected set; }

		public DfaState(int index, int edgecount, int acceptsymbol)
		{
			Index = index;
			AcceptSymbol = acceptsymbol;
			EdgeCount = edgecount;
			Edges = new List<Edge>();
		}

	}

	public class DFATable
	{
		public List<DfaState> States;

		public DFATable()
		{
			States = new List<DfaState>();
		}
	}
}
