﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGoldEngine
{
	public class Edge
	{
		public int CharSetIndex { get; protected set; }
		public int Target { get; protected set; }

		public Edge(int charsetindex, int target)
		{
			CharSetIndex = charsetindex;
			Target = target;
		}

	}
}
