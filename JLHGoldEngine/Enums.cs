﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGoldEngine
{
	public enum SymbolType
	{
		EOF = 3,
		Error = 7,
		Space = 2,
		Content = 1,
		NonTerminal = 0,
		Null = 255
	}

	public enum ParseMessage
	{
		TokenRead = 0,
		Reduction = 1,
		LexAccept = 2,
		ParseAccept = 3,
		LexicalError = 4,
		SyntaxError = 5,
		LexEmpty = 6,
		Null = 255
	}

	public enum LalrResult
	{
		Accept = 1,
		Shift = 2,
		ReduceNormal = 3,
		ReduceTrim = 4,
		SyntaxError = 5,
		Null = 255
	}

	public enum LrActionType
	{
		Shift = 1,
		Reduce = 2,
		Goto = 3,
		Accept = 4,
		Error = 5,
		None = 255,
	}

}
