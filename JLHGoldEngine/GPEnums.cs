﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGoldEngine
{
	public enum SymbolConstants
	{
		symbol_eof = 0, // EOF
		symbol_error = 1, // Error
		symbol_whitespace = 2, // whitespace
		symbol_minus = 3,
		symbol_lparen = 4,
		symbol_rparen = 5,
		symbol_mult = 6, // *
		symbol_comma = 7,
		symbol_div = 8, // /
		symbol_plus = 9,
		symbol_avg = 10,
		symbol_cellref = 11,
		symbol_intconstant = 12,
		symbol_sum = 13,
		symbol_expression = 14,
		symbol_formula = 15,
		symbol_function = 16,
		symbol_mult_exp = 17,
		symbol_negate_exp = 18,
		symbol_value = 19,
		symbol_valuelist = 20,
	}

	public enum RuleConstants
	{
		formula_expr = 0,
		expr_expr_plus_multexpr = 1,
		expr_expr_minus_multexpr = 2,
		expr_multexpr = 3,
		multexpr_multexpr_times_negateexpr = 4,
		multexpr_multexpr_div_negateexpr = 5,
		multexpr_negateexpr = 6,
		negateexpr_minus_value = 7,
		negateexpr_value = 8,
		value_cellref = 9,
		value_parenexpr = 10,
		value_intconstant = 11,
		value_function = 12,
		valuelist_value = 13,
		valuelist_value_comma_valuelist = 14,
		function_sum = 15,
		function_avg = 16,
	}
}
