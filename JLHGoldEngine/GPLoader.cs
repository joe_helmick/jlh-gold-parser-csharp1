﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Text;

namespace JLHGoldEngine
{
	public class GPLoader
	{
		private readonly string _xmlpath;
		public GPLoader(string xmlpath)
		{
			_xmlpath = xmlpath;
		}

		public void Load()
		{
			var fs = new FileStream(_xmlpath, FileMode.Open, FileAccess.Read);
			var sr = new StreamReader(fs);
			string xml = sr.ReadToEnd();
			sr.Close();
			fs.Close();

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(xml);

			XmlNode root = doc.DocumentElement;

			// LOAD PROPERTIES (DIAGNOSTIC/DEBUG ONLY)
			var properties = root.SelectNodes("descendant::Property");
			foreach (XmlNode prop in properties)
			{
				string value = prop.Attributes["Value"].Value;
				Console.WriteLine(value);
			}
			Console.WriteLine("-------------------------------------------------------------");


			// LOAD SYMBOLS
			var symbols = root.SelectNodes("descendant::Symbol");
			foreach (XmlNode s in symbols)
			{
				int index = Convert.ToInt32(s.Attributes["Index"].Value);
				string name = s.Attributes["Name"].Value;
				int symtype = Convert.ToInt32(s.Attributes["Type"].Value);
				Symbol sym = new Symbol(index, name, (SymbolType)symtype);
				V.SymbolsList.List.Add(sym);
			}

			// LOAD CHARACTER SETS
			var charsets = root.SelectNodes("descendant::CharSet");
			foreach (XmlNode cs in charsets)
			{
				int index = Convert.ToInt32(cs.Attributes["Index"].Value);
				int count = Convert.ToInt32(cs.Attributes["Count"].Value);
				Charset cset = new Charset(index, count);

				XmlNodeList chars = cs.SelectNodes("descendant::Char");

				if (index == 0) // special treatment for white space character set
				{
					cset.Chars.Add(' ');  // blow off all the rest
				}
				else
				{
					foreach (XmlNode ch in chars)
					{
						string charstring = ch.Attributes["UnicodeIndex"].Value;
						int charint = Int32.Parse(charstring);
						char character = (char)charint;
						cset.Chars.Add(character);
					}
				}
				V.CharsetList.List.Add(cset);
			}

			// LOAD DFA STATES
			XmlNode dfatable = root.SelectSingleNode("descendant::DFATable");
			XmlNodeList dfastates = dfatable.SelectNodes("descendant::DFAState");
			foreach (XmlNode dfastate in dfastates)
			{
				int index = Convert.ToInt32(dfastate.Attributes["Index"].Value);
				int accept = Convert.ToInt32(dfastate.Attributes["AcceptSymbol"].Value);
				int ecount = Convert.ToInt32(dfastate.Attributes["EdgeCount"].Value);
				DfaState state = new DfaState(index, ecount, accept);

				XmlNodeList edges = dfastate.SelectNodes("descendant::DFAEdge");

				foreach (XmlNode edge in edges)
				{
					int charsetindex = Convert.ToInt32(edge.Attributes["CharSetIndex"].Value);
					int target = Convert.ToInt32(edge.Attributes["Target"].Value);
					Edge e = new Edge(charsetindex, target);
					state.Edges.Add(e);
				}
				V.DfaTable.States.Add(state);
			}

			// LOAD PRODUCTIONS
			XmlNode xmlprodtable = root.SelectSingleNode("descendant::m_Production");
			XmlNodeList xmlproductions = xmlprodtable.SelectNodes("descendant::Production");
			foreach (XmlNode xmlproduction in xmlproductions)
			{
				int index = Convert.ToInt32(xmlproduction.Attributes["Index"].Value);
				int nti = Convert.ToInt32(xmlproduction.Attributes["NonTerminalIndex"].Value);
				int scount = Convert.ToInt32(xmlproduction.Attributes["SymbolCount"].Value);
				Production production = new Production(index, nti, scount);
				XmlNodeList xmlps = xmlproduction.SelectNodes("descendant::ProductionSymbol");
				StringBuilder sbExpansion = new StringBuilder(V.SymbolsList.List[production.NonTerminalIndex].Name + " ::= ");	
				foreach (XmlNode nps in xmlps)
				{
					int symindex = Convert.ToInt32(nps.Attributes["SymbolIndex"].Value);
					sbExpansion.Append(V.SymbolsList.List[symindex].Name + " ");
					production.ProductionSymbols.Add(symindex);
				}
				production.Expansion = sbExpansion.ToString();
				V.ProductionList.Productions.Add(production);
			}

			// LOAD LALR STATES
			XmlNode xmllalrtable = root.SelectSingleNode("descendant::LALRTable");
			XmlNodeList xmllalrstates = xmllalrtable.SelectNodes("descendant::LALRState");
			foreach (XmlNode xmlstate in xmllalrstates)
			{
				int index = Convert.ToInt32(xmlstate.Attributes["Index"].Value);
				int acount = Convert.ToInt32(xmlstate.Attributes["ActionCount"].Value);
				LalrState state = new LalrState(index, acount);

				XmlNodeList xmlas = xmlstate.SelectNodes("descendant::LALRAction");

				foreach (XmlNode a in xmlas)
				{
					int symindex = Convert.ToInt32(a.Attributes["SymbolIndex"].Value);
					int action = Convert.ToInt32(a.Attributes["Action"].Value);
					int value = Convert.ToInt32(a.Attributes["Value"].Value);
					LalrAction act = new LalrAction(symindex, action, value);
					state.Actions.Add(act);
				}
				V.LalrStateList.States.Add(state);
			}

		}
	}
}
