﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGoldEngine
{
	public class LalrState
	{
		public int Index { get; set; }
		public int ActionCount { get; set; }



		public List<LalrAction> Actions = new List<LalrAction>();

		public LalrState(int index, int actionCount)
		{
			Index = index;
			ActionCount = actionCount;
			Actions = new List<LalrAction>();
		}


	}

	public class LalrStateList
	{
		public List<LalrState> States;

		public LalrStateList()
		{
			States = new List<LalrState>();
		}
	}

	public class LalrAction
	{
		public int SymbolIndex { get; set; }
		public int Action { get; set; }
		public int Value { get; set; }

		public LalrAction(int symbolIndex, int action, int value)
		{
			SymbolIndex = symbolIndex;
			Action = action;
			Value = value;
		}

	}

}
