﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGoldEngine
{
	public class Node
	{
		public int Index { get; protected set; }
		public List<Node> Children;

		public Node(int index)
		{
			Index = index;
			Children = new List<Node>();
		}
	}


}
