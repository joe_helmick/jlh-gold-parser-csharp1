﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JLHGoldEngine
{
	#region LL1 RECURSIVE DESCENT PARSER
	public static class LL1RDParser
	{
		private static int _tokenIndex;
		private static Token _currentToken;

		public static void Formula()
		{
			_tokenIndex = 0;
			if (V.TokenList.Count > 0)
			{
				_currentToken = V.TokenList[_tokenIndex];
				Expr();
			}
		}

		public static void Expr()
		{
			Term();
			Expr1();
		}

		public static void Expr1()
		{
			if (_currentToken.Index == 9) // + add
			{
				_tokenIndex++;
				Term();
				Expr1();
			}

			if (_currentToken.Index == 3) // - subtract. negate
			{
				_tokenIndex++;
				Term();
				Expr1();
			}
		}

		public static void Term()
		{
			Factor();
			Term1();
		}

		public static void Term1()
		{
			if (_currentToken.Index == 6) // * mult
			{
				_tokenIndex++;
				Factor();
				Term1();
			}

			if (_currentToken.Index == 8) // / divide
			{
				_tokenIndex++;
				Factor();
				Term1();
			}
		}

		public static void Factor()
		{
			if (_currentToken.Index == 4) // (
			{
				_tokenIndex++; // (
				Expr();
				_tokenIndex++; // )
			}

			if ((int) _currentToken.Index == 14) // numlit
			{
				Console.WriteLine(_currentToken.Text);
			}

			if ((int) _currentToken.Index == 11) // cellref
			{
				Console.WriteLine(_currentToken.Text);
			}

			if ((int) _currentToken.Index == 15) // SUM(
				Function();

			if ((int) _currentToken.Index == 10) // AVG(
				Function();

			if ((int) _currentToken.Index == 13) // MIN(
				Function();

			if ((int) _currentToken.Index == 12) // MAX(
				Function();

		}

		public static void ExprList()
		{
			Expr();
			ExprList2();
		}

		public static void ExprList2()
		{
			if (_currentToken.Index == 7) // , comma list separator
			{
				ExprList();
			}

		}

		public static void Function()
		{
			if (_currentToken.Index == 15) // SUM(
			{
				Sum();
			}

			if (_currentToken.Index == 10) // AVG(
			{
				Avg();
			}

			if (_currentToken.Index == 13) // MIN(
			{
				Min();
			}

			if (_currentToken.Index == 12) // MAX(
			{
				Max();
			}

		}

		public static void Sum()
		{
		}

		public static void Avg()
		{
		}

		public static void Min()
		{
		}

		public static void Max()
		{
		}

	}
	#endregion



	public static class LALR1Parser
	{ 
	//==============================================================================================
		//public static void Tokenize()
		//{
		//	LookAheadDfa();

		//	if (V.tokenSymbolType == SymbolType.EOF)
		//		goto endOfFile;

		//	if (V.tokenSymbolType == SymbolType.Error)
		//		goto lexError;

		//	// Not error and not EOF, so must be a good token.
		//	V.inputBufPtr += V.tokenText.Length;
		//	V.inputBufPos = 0;
		//	V.parseMessage = ParseMessage.TokenRead;
		//	Token t = new Token(V.tokenSymbolIdx, V.tokenText, V.tokenSymbolType);
		//	t.Text = V.tokenText;
		//	V.TokenList.Add(t);
		//	return;

		//	lexError:
		//	// DFA returned lexical error.
		//	V.parseMessage = ParseMessage.LexicalError;
		//	return;

		//	endOfFile:
		//	// If we reach EOF and haven't gone anywhere, then input is empty.
		//	if (V.inputBufPtr == 0)
		//		goto emptyInput;

		//	// If we reach EOF and haven't encountered an error, then we're done.
		//	V.parseMessage = ParseMessage.LexAccept;
		//	return;

		//	emptyInput:
		//	V.parseMessage = ParseMessage.LexEmpty;
		//}
		// ======================================================================================================


		// ======================================================================================================
		private static void LookAheadDfa()
		{
			V.lastAcceptState = V.NEG1;
			V.lastAcceptPosition = V.NEG1;
			V.currentDfa = 0;

			LookAhead(0);
			if (V.chLookAhead == '\u0000')
				goto eofReached1;

LookAheadDfaLoopTop:

			LookAhead(V.inputBufPos);
			if (V.chLookAhead == '\u0000')
				goto targetNotFound;

			V.targetFound = false;

			int edgeCount = V.DfaTable.States[V.currentDfa].EdgeCount;
			int b = 0;
			Edge edge = null;
			bool edgeHasCharacter;
			while (b < edgeCount)
			{
				edge = V.DfaTable.States[V.currentDfa].Edges[b];
				edgeHasCharacter = V.CharsetList.List[edge.CharSetIndex].Chars.Contains(V.chLookAhead);
				if (edgeHasCharacter)
				{
					V.targetFound = true;
					break;
				}
				b++;
			}

			if (V.targetFound == true)
			{
				V.target = edge.Target;
				goto targetFound;
			}
				goto targetNotFound;


targetFound:
			if (V.DfaTable.States[V.target].AcceptSymbol == V.NEG1)
				goto endTargetFound;
			V.lastAcceptState = V.target;
			V.lastAcceptPosition = V.inputBufPos;
V.tokenSymbolIdx = V.DfaTable.States[V.target].AcceptSymbol;
			// fall through

		endTargetFound:
			V.inputBufPos++;
			V.currentDfa = V.target;
			goto LookAheadDfaLoopTop;

targetNotFound:
			if(V.lastAcceptState == V.NEG1)
				goto tnflasneg1;

			DfaState state = V.DfaTable.States[V.lastAcceptState];
			V.tokenSymbolIdx = state.AcceptSymbol;
			Symbol symbol = V.SymbolsList.List[state.AcceptSymbol];
			V.tokenSymbolType = symbol.SymbolType;
			V.tokenLength = V.lastAcceptPosition + 1;
			V.tokenFilled = true;
			CopyTokenText();
			goto done;

tnflasneg1:
			V.tokenFilled = true;
			V.tokenSymbolIdx = 1; // index of error symbol
			V.tokenSymbolType = SymbolType.Error;
			CopyTokenText();
			// fall through to done
done:
			return;

eofReached1:
			V.tokenFilled = true;
			V.tokenSymbolIdx = 0; // index of EOF symbol
			V.tokenData = 0;
			V.tokenText = "";
			V.tokenSymbolType = SymbolType.EOF;
			return;
		}
		// ======================================================================================================


		// ======================================================================================================
		private static void LookAhead(int a)
		{
			int position = V.inputBufPtr + a;
			V.chLookAhead = V.INPUT_BUFFER[position];
		}
		// ======================================================================================================


		// ======================================================================================================
		private static void CopyTokenText()
		{
			int b = V.tokenLength;
			int n = 0;
			char[] chars = new char[b];

			// clear out the token text first
			V.tokenText = "";

			// copy characters from input buffer to token text field
loopCopyTokenText:
			chars[n] = V.INPUT_BUFFER[V.inputBufPtr + n];
			n++;
			b--;
			if(b != 0)
				goto loopCopyTokenText;

			V.tokenText = new string(chars);
		}


		// ======================================================================================================
		public static void RunLALR()
		{

			// find the new action symbol indexes match
			V.lrActionValue = V.NEG1;

			// look for the token
			foreach (var action in V.LalrStateList.States[V.curLalrState].Actions)
			{
				if (action.SymbolIndex == V.tokenSymbolIdx)
				{
					V.lrActionValue = action.Value;
					V.lrActionType = (LrActionType)action.Action;
					break;
				}
			}

			if (V.lrActionValue == V.NEG1)
				goto runLalrError;

//			V.haveReduction = false;

			if (V.lrActionType == LrActionType.Accept)
				goto runLalrAccept;

			if (V.lrActionType == LrActionType.Shift)
				goto runLalrShift;

			if (V.lrActionType == LrActionType.Reduce)
				goto runLalrTryTrim;

			Console.WriteLine("BAD DISPATCH MARKER 2 SHOULDN'T GET HERE");
			return;

runLalrTryTrim:

			// Create a new blank production.
			Production prod = new Production();

			Token head = new Token();

			// Search the table to find its values.
			foreach (Production production in V.ProductionList.Productions)
			{
				// Found the right one, so fill in its values.
				if (production.Index == V.lrActionValue)
				{
					// found the right one, save its values
					prod.Index = production.Index;
					prod.NonTerminalIndex = production.NonTerminalIndex;
					prod.SymbolCount = production.SymbolCount;
					// copy the production symbols (children) as well
					foreach (var ps in V.ProductionList.Productions[V.lrActionValue].ProductionSymbols)
					{
						prod.ProductionSymbols.Add(ps);
					}
					break;
				}
			}

			// if we don't have a single production symbol then we must do regular reduce
			if (prod.SymbolCount != 1)
				goto runLalrReduce;

			// if we get here, we only have one production symbol, so get its index.
			int pSymOnly = prod.ProductionSymbols[0];

			// and now test it to see if it's a nonterminal; find the matching symbol in the symbols list
			foreach (Symbol sym in V.SymbolsList.List)
			{
				if (sym.Index == pSymOnly)
				{
					// found it, test its type
					if (sym.SymbolType != SymbolType.NonTerminal)
						// not nonterminal, so we cannot trim
						goto runLalrReduce;
				}
			}

			// if we get here we have one non terminal symbol, so can do trimmed reduction
			head = V.TokenStack.Pop();
			head.Index = prod.Index;
			V.lastLalrResult = LalrResult.ReduceTrim;
			goto finishReduce;


runLalrReduce:

//			V.haveReduction = true;
			Reduction newReduction = new Reduction();
			newReduction.Production = prod;
			for (int nm = 0; nm < prod.SymbolCount; nm++)
			{
				newReduction.Tokens.Add(V.TokenStack.Pop());
			}
//			head = new Token();
			head.Index = prod.Index;
			head.Data = newReduction;
			V.lastLalrResult = LalrResult.ReduceNormal;
		// fall through

finishReduce:
			int curTokenState = V.TokenStack.Peek().State;
			V.lrActionIndex = V.NEG1;
			// Search the LALRState's action list to find the one whose symbol matches the production's nonterminal symbol.
			int b = 0;
			foreach (LalrAction action in V.LalrStateList.States[curTokenState].Actions)
			{
				if (action.SymbolIndex == prod.NonTerminalIndex)
				{
					V.lrActionIndex = b;
					goto finishReduce2;
				}
				b++;
			}

finishReduce2:
			V.curLalrState = V.LalrStateList.States[curTokenState].Actions[V.lrActionIndex].Value;
			head.State = V.curLalrState;
			V.TokenStack.Push(head);
			return;

runLalrShift:
			V.curLalrState = V.lrActionValue;
//			V.tokenState = V.lrActionValue;
			// push new token onto stack
			Token t = new Token(V.tokenSymbolIdx, V.tokenText, V.tokenSymbolType);
			t.State = V.curLalrState;
			V.TokenStack.Push(t);
//			V.tokenState = V.curLalrState;
			V.lastLalrResult = LalrResult.Shift;
			return;

runLalrAccept:
//			V.haveReduction = true;
			V.lastLalrResult = LalrResult.Accept;
			return;

runLalrError:
			V.lastLalrResult = LalrResult.SyntaxError;
			return;
		}
		//======================================================================================================


		//======================================================================================================
//		public static void PrintReduction(int indent, Reduction reduction)
//		{
//			int indentlevel = indent;
////			const string oneindent = "\u00B7\u00B7\u00B7\u00B7\u00B7\u00B7";
//			const string oneindent = "\u2500\u2500\u2500\u2500\u2524";
//			StringBuilder sb = new StringBuilder();
//			sb.Append("  ");
//			for (int n = 1; n <= indentlevel; n++)
//				sb.Append(oneindent);

//			string expansion = V.ProductionList.Productions[reduction.Production.Index].Expansion;
//			Console.WriteLine(sb.ToString() + " prod " + reduction.Production.Index + "  " + expansion);

//			foreach (Token t1 in reduction.Tokens)
//			{
//				if (t1.Data != null)
//				{
//					if (t1.Data.GetType() == typeof(Reduction))
//					{
//						indentlevel++;
//						Reduction r2 = (Reduction)t1.Data;
//						PrintReduction(indentlevel, r2);
//					}
//				}
//				else
//				{
//					Console.WriteLine(sb.ToString() + " " + t1.Name);
//				}

//			}
//		}
		//======================================================================================================


		//======================================================================================================
		public static void ParseLALR1()
		{
			// Check for null input.
			LookAhead(0);
			if (V.chLookAhead == '\u0000')
				goto endOfFile;

			loopParse:

			if (V.tokenFilled == false)
				goto noToken;

			if (V.tokenSymbolType == SymbolType.Error)
				goto lexError;

			RunLALR();

			if (V.lastLalrResult == LalrResult.Accept)
				goto lalrAccept;

			if (V.lastLalrResult == LalrResult.ReduceNormal)
				goto lalrReduceNormal;

			if (V.lastLalrResult == LalrResult.ReduceTrim)
				goto lalrReduceTrim;

			if (V.lastLalrResult == LalrResult.SyntaxError)
				goto lalrSynError;

			if (V.lastLalrResult == LalrResult.Shift)
				goto lalrShift;

			return;

		lalrShift:
			Console.WriteLine("shift lalr state $" + V.curLalrState.ToString("X").PadLeft(2, '0') + " " + V.curLalrState);
			// clear out input token so next pass will read one
			V.tokenLength = 0;
			V.tokenSymbolIdx = 0;
			V.tokenSymbolType = SymbolType.Null;
			V.tokenData = 0;
//			V.tokenState = 0;
			V.tokenFilled = false;
			V.tokenText = "";
			V.lastLalrResult = LalrResult.Shift;
			goto loopParse;

		lalrSynError:
			V.parseMessage = ParseMessage.SyntaxError;
			return;

		lalrReduceNormal:
			V.parseMessage = ParseMessage.Reduction;
			Console.WriteLine("reduce lalr state $" + V.curLalrState.ToString("X").PadLeft(2, '0') + " " + V.curLalrState);
			goto loopParse;

		lalrReduceTrim:
			V.parseMessage = ParseMessage.Reduction;
			Console.WriteLine("reduce lalr state $" + V.curLalrState.ToString("X").PadLeft(2, '0') + " " + V.curLalrState + " trimmed");
			goto loopParse;

		lalrAccept:
			V.parseMessage = ParseMessage.ParseAccept;
			Console.WriteLine("accept lalr state $" + V.curLalrState.ToString("X").PadLeft(2, '0') + " " + V.curLalrState);
			return;

		lexError:
			V.parseMessage = ParseMessage.LexicalError;
			return;

		noToken:
			LookAheadDfa();
			int count = V.tokenText.Length;
			V.inputBufPtr += count;
			V.inputBufPos = 0;

			V.parseMessage = ParseMessage.TokenRead;
			Console.WriteLine("token read " + V.tokenText + " lalr state $" + V.curLalrState.ToString("X").PadLeft(2, '0') + " " + V.curLalrState);
			return;

		endOfFile:
			if (V.curLalrState == 0)
				goto emptyInput;
			goto loopParse;

		emptyInput:
			V.parseMessage = ParseMessage.LexEmpty;
		}
		//======================================================================================================

}




}
