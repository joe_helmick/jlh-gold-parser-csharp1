﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGoldEngine
{
	public class Production
	{
		public int Index { get; set; }
		public int NonTerminalIndex { get; set; }
		public int SymbolCount { get; set; }
		public List<int> ProductionSymbols = new List<int>();
		public string Expansion { get; set; }

		public Production(int index, int nonTerminalIndex, int symbolCount)
		{
			Index = index;
			NonTerminalIndex = nonTerminalIndex;
			SymbolCount = symbolCount;
			ProductionSymbols = new List<int>();
		}

		public Production()
		{
		}

	}

	public class ProductionList
	{
		public List<Production> Productions = new List<Production>();

		public ProductionList()
		{
			Productions = new List<Production>();
		}
	}


}
