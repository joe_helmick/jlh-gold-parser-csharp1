﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGoldEngine
{
	public class Reduction
	{
		public Production Production { get; set; }
		public List<Token> Tokens { get; set; }

		public Reduction()
		{
			Production = new Production();
			Tokens = new List<Token>();
		}
	}
}
