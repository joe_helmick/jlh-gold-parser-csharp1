﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGoldEngine
{
	public class Symbol
	{
		public int Index { get; set; }
		public string Name { get; private set; }
		public SymbolType SymbolType { get; set; }

		public Symbol(int index, string name, SymbolType symboltype)
		{
			Index = index;
			Name = name;
			SymbolType = symboltype;
		}

		public Symbol()
		{
		}

	}


	public class SymbolsList
	{
		public List<Symbol> List { get; set; }

		public SymbolsList()
		{
			List = new List<Symbol>();
		}
	}


}


