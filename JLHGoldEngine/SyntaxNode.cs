﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLHGPDriver
{
	public class ASTNode
	{
		public virtual int Value => 0;
	}

	public class NodeInt : ASTNode
	{
		private int _i;

		public NodeInt(int i)
		{
			_i = i;
		}

		public int Value
		{
			get { return _i; }
		}

	}

	public class NodeBinOp : ASTNode
	{
		private int _op, _lhs, _rhs;

		public NodeBinOp(int lhs, int op, int rhs)
		{
			_lhs = lhs;
			_op = op;
			_rhs = rhs;
		}

		public int Value
		{
			get
			{ 
				if (_op == 3) // -
					return _lhs - _rhs;

				if (_op == 6) // *
					return _lhs * _rhs;

				if (_op == 8) // /
					return _lhs / _rhs;

				if (_op == 9) // +
					return _lhs + _rhs;

				return 0;
			}
		}
	}

}



