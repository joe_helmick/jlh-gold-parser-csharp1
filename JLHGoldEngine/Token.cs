﻿namespace JLHGoldEngine
{
	public class Token : Symbol
	{
		public int State { get; set; }
		public object Data { get; set; }
		public string Text { get; set; }
		public bool Filled { get; set; }

		public Token(int index, string name, SymbolType symboltype)
			:base(index, name, symboltype)
		{
			
		}

		public Token()
		{
		}

	}
}
