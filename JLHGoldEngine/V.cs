﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using JLHGPDriver;

namespace JLHGoldEngine
{

/*

"Name"     = 'jsheet3.grm'
"Author"   = 'Joe Helmick'
"Version"  = '2019-06-01a'
"About"    = ''
"Start Symbol" = <AddExp>
"Case Sensitive" = 'True'
                
!"Auto Whitespace" = 'False'
! -------------------------------------------------
! Character Sets
! -------------------------------------------------
{UCLetter}       = [ABCDEFGHIJ]
! -------------------------------------------------
! Terminals
! -------------------------------------------------
CellRef  = {UCLetter}{Digit}+
Number   = {Digit}+
! -------------------------------------------------
! Rules
! -------------------------------------------------

<AddExp>  ::= <AddExp> '+' <MultExp> 
           |  <AddExp> '-' <MultExp> 
           |  <MultExp> 

<MultExp> ::= <MultExp> '*' <NegExp> 
           |  <MultExp> '/' <NegExp> 
           |  <NegExp> 

<NegExp>  ::= '-' <Value> 
           |  <Value> 

<Value> ::= CellRef
          | Number
          | '(' <AddExp> ')'

 */

	public static class V
	{
		public const string GPXML_FILE = @"D:\dev\Z80-ROMs\GoldParser01\jsheet3.xml";

		public const int NEG1 = -1;

		// In-memory tables.  
		public static SymbolsList SymbolsList = new SymbolsList();
		public static CharsetList CharsetList = new CharsetList();
		public static DFATable DfaTable = new DFATable();
		public static ProductionList ProductionList = new ProductionList();
		public static LalrStateList LalrStateList = new LalrStateList();

		// The test input string.
		public const string INPUT_BUFFER = "-A1+7/11\u0000";
//		public const string INPUT_BUFFER = "\u0000";

		// The main parser result variable.
		public static ParseMessage parseMessage = ParseMessage.Null;
		
		// DFA variables.
		public static int currentDfa;
		public static int inputBufPos = 0;
		public static int inputBufPtr = 0;
		public static int lastAcceptState;
		public static int lastAcceptPosition;
		public static char chLookAhead;
		public static bool targetFound;
		public static int target;

		// Token variables.
		public static int tokenLength;
		public static int tokenSymbolIdx;
		public static SymbolType tokenSymbolType;
		public static int tokenData;
//		public static int tokenState;
		public static bool tokenFilled;
		public static string tokenText;

		// LALR variables.
		public static LalrResult lastLalrResult;
		public static int curLalrState;
		public static LrActionType lrActionType;
		public static int lrActionValue;
		public static int lrActionIndex;
//		public static bool haveReduction;


		public static List<Token> TokenList = new List<Token>();
		public static Stack<Token> TokenStack = new Stack<Token>();

		public static List<ASTNode> ASTList = new List<ASTNode>();

	}
}
